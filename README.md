# Fanaba official website

Fanaba is an Iranian startup which is active in Finance and Cryptography field. Fanaba is trying to create a Free-Libre economic platform.

# Requirements to contribute this project

To contribute this project, You will need to know some technologies. Here we listed these technologies:

- HTML5 & CSS3
- JavaScript & jQuery
- npm (Node Package Manager)
- SASS (.scss Format)
- Webpack CLI

# Project Directory Structure

- The output directory for CSS files is dist/stylesheet
- The output directory for JS files is dist/javascript
- CSS Libraries & JavaScript Frameworks could be imported from 'static/lib'
- You've to use SASS to watch and export the changes from 'dev/stylesheet' to output directory.
- You've to use Webpack to watch and export the changes from 'dev/javascript/ to output directory.
- We need to keep node_modules directory in standard structure, so you have to use npm (Node Package Manager).
